package com.org.hazedine.DepartementService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
@RefreshScope
public class DepartementApplication {


    @Value("${server.port}")
    String serverport ;
   @Value("${spring.datasource.url}")
    String spring_datasource_url;

   //@Value("${management.endpoints.web.exposure.include}")
   String actuatorendpoint;

   
    @GetMapping("/params")
    public String params(){
        return toString();

    }

    @Override
    public String toString() {
        return "DepartementApplication{" +
                "serverport='" + serverport + '\'' +
                ", spring_datasource_url='" + spring_datasource_url + '\'' +
                ", actuatorendpoint='" + actuatorendpoint + '\'' +
                '}';
    }

    public static void main(String[] args) {

		SpringApplication.run(DepartementApplication.class, args);
	}

}
