package com.org.hazedine.DepartementService.dao;

import com.org.hazedine.DepartementService.domain.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface DepartementRepository extends JpaRepository<Departement,Long> {
}
