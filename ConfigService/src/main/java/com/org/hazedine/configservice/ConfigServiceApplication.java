package com.org.hazedine.configservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServiceApplication implements CommandLineRunner {
    @Value("${spring.cloud.config.server.git.uri}")
    String uri ;

	public static void main(String[] args) {
		SpringApplication.run(ConfigServiceApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        System.out.println("uri : " +uri );
    }
}
