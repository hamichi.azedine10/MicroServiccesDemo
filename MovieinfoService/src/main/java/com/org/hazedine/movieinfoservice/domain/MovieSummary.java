package com.org.hazedine.movieinfoservice.domain;

public class MovieSummary {
    private  String original_title;
    private  String overview;
    private String original_language;
    private  double popularity;

    public MovieSummary(String original_title, String overview, String original_language, double popularity) {
        this.original_title = original_title;
        this.overview = overview;
        this.original_language = original_language;
        this.popularity = popularity;
    }

    public MovieSummary() {
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return "MovieSummary{" +
                "original_title='" + original_title + '\'' +
                ", overview='" + overview + '\'' +
                ", original_language='" + original_language + '\'' +
                ", popularity=" + popularity +
                '}';
    }
}
