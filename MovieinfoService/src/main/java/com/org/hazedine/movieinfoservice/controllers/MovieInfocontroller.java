package com.org.hazedine.movieinfoservice.controllers;

import com.org.hazedine.movieinfoservice.domain.MovieInfo;


import com.org.hazedine.movieinfoservice.domain.MovieSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/movies")
public class MovieInfocontroller {

    @Value("${api.thmovieinfo.key}")
    private  String apikey;

    @Autowired
    RestTemplate restTemplate;
    @GetMapping("/{movieId}")
    public MovieInfo getmovieinfo(@PathVariable("movieId") String movieId){
        MovieSummary movieSummary = restTemplate.getForObject("https://api.themoviedb.org/3/movie/"+movieId+"?api_key="+apikey,MovieSummary.class);
        System.out.println("movieSummary  = "+movieSummary);
        return new MovieInfo(movieId,movieSummary.getOriginal_title(),movieSummary.getOverview());
    }
}
