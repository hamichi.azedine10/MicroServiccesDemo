package com.org.hazedine.moviecatalog.domain;

public class CatalogeItem {
    String name ;
    String details;
    Integer rating;

    public CatalogeItem(String name, String details, Integer rating) {
        this.name = name;
        this.details = details;
        this.rating = rating;
    }

    public CatalogeItem() {
    }

    @Override
    public String toString() {
        return "CatalogeItem{" +
                "name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
