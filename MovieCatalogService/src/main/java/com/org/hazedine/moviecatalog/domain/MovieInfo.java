package com.org.hazedine.moviecatalog.domain;

public class MovieInfo {
    String movieId;
    String moviename;
    String detail;

    public MovieInfo(String movieId, String moviename, String detail) {
        this.movieId = movieId;
        this.moviename = moviename;
        this.detail = detail;
    }

    public MovieInfo() {
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMoviename() {
        return moviename;
    }

    public void setMoviename(String moviename) {
        this.moviename = moviename;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "MovieInfo{" +
                "movieId='" + movieId + '\'' +
                ", moviename='" + moviename + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
