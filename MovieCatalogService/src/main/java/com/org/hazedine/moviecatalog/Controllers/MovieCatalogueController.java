package com.org.hazedine.moviecatalog.Controllers;

import com.org.hazedine.moviecatalog.domain.CatalogeItem;
import com.org.hazedine.moviecatalog.domain.MovieInfo;
import com.org.hazedine.moviecatalog.domain.Rating;

import com.org.hazedine.moviecatalog.domain.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/getmovies")
public class MovieCatalogueController{

    @Autowired
    RestTemplate restTemplate;


    @GetMapping("/{userId}")
    List<CatalogeItem>ListCatalogItems(@PathVariable("userId")String userId){
       // recuperer les infos depuis  RATINGSDATASERVICE
      UserRating userRating = restTemplate.getForObject("http://RATINGSDATASERVICE/ratings/users/"+userId ,UserRating.class);


       List<CatalogeItem> catalogeItems=  userRating.getRatinguser().stream().map(rating ->
              {
                  // recuperer les info(movieinfo) depuis le microservice MovieInfoService
                  MovieInfo movieInfo = restTemplate.getForObject("http://MOVIEINFOSERVICE/movies/"+rating.getMovieId(), MovieInfo.class);
                  // creer CatalogeItem avec les information recuperées depuis les autres services
                  return new CatalogeItem(movieInfo.getMoviename(),movieInfo.getDetail(),rating.getRating());
              }
              ).collect(Collectors.toList());
        //returner la liste
    return catalogeItems;
    }
}
