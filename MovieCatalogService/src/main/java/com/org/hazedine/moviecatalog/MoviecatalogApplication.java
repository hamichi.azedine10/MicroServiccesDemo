package com.org.hazedine.moviecatalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RefreshScope
@EnableDiscoveryClient
//todo : obsolete
//@EnableCircuitBreaker
public class MoviecatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviecatalogApplication.class, args);
	}


	@Bean
    @LoadBalanced
    RestTemplate restTemplateloadbalanced (){
	    return new RestTemplate();
    }

}
