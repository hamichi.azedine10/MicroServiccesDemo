package com.org.hazedine.moviecatalog.domain;

import java.util.List;

public class UserRating {

    List<Rating> ratinguser;

    public UserRating(List<Rating> ratinguser) {
        this.ratinguser = ratinguser;
    }

    public UserRating() {
    }

    public List<Rating> getRatinguser() {
        return ratinguser;
    }

    public void setRatinguser(List<Rating> ratinguser) {
        this.ratinguser = ratinguser;
    }

    @Override
    public String toString() {
        return "UserRating{" +
                "ratinguser=" + ratinguser +
                '}';
    }
}
