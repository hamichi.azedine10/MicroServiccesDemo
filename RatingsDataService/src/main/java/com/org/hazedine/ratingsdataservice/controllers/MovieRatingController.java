package com.org.hazedine.ratingsdataservice.controllers;

import com.org.hazedine.ratingsdataservice.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/ratings")
public class MovieRatingController {
    @Autowired
    UserRating userRating;

    @GetMapping("/{movieId}")
    public List<Rating> getMovies(@PathVariable("movieId") String movieId){
      return Arrays.asList(new Rating(movieId,4));
    }
    @GetMapping("users/{userId}")
    public  UserRating getUserRating(@PathVariable("userId") String userId){
        List<Rating> ratingList = Arrays.asList(
                new Rating("200",4),
                new Rating("300",3),
                new Rating("500",5));
        userRating.setRatinguser(ratingList);
        return userRating;
    }
}
