package com.org.hazedine.ratingsdataservice;

import com.org.hazedine.ratingsdataservice.domain.UserRating;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
public class RatingsdataserviceApplication {

    @Bean
    UserRating userRating(){
        return new UserRating();
    }

	public static void main(String[] args) {
		SpringApplication.run(RatingsdataserviceApplication.class, args);
	}

}
